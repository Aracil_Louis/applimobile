package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{


     /*@Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);
         final ListView listview = (ListView) findViewById(R.id.lst);
         final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());
         listview.setAdapter(adapter);
         listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             public void onItemClick(AdapterView parent, View v,int position,long id) {

                 Intent animal = new Intent(MainActivity.this, AnimalActivity.class);
                 animal.putExtra("key", (String) parent.getItemAtPosition(position));
                 startActivity(animal);
             }
         });
     }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(new IconicAdapter());

    }


    public class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater()
                    .inflate(R.layout.layout_row, parent,false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder,int position) {
            holder.bindModel(AnimalList.getNameArray()[position]);
        }
        @Override
        public int getItemCount() {
            return(AnimalList.getNameArray().length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView label = null;
        ImageView icon = null;

        RowHolder(View row) {
            super(row);
            label = (TextView) row.findViewById(R.id.text);
            icon = (ImageView) row.findViewById(R.id.icon);
            label.setOnClickListener(this);
            icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final String item = label.getText().toString();
            Intent animal = new Intent(MainActivity.this, AnimalActivity.class);
            animal.putExtra("key", (String) label.getText());// recupérer le texte de la ligne,pas la ligne elle même (la vue)
            startActivity(animal);

        }

        void bindModel(String item) {
            label.setText(item);
            final Animal monAnimal = AnimalList.getAnimal(item);
            int id = getResources().getIdentifier(monAnimal.getImgFile(), "drawable", getPackageName());
            icon.setImageResource(id);
        }
    }

}
