package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animals_details);

        Intent animal = getIntent();
        String key= animal.getStringExtra("key");
        final TextView animalName = (TextView) findViewById(R.id.name);
        animalName.setText(key);

        final Animal monAnimal = AnimalList.getAnimal(key);

        Toast.makeText(animalName.getContext(), "Le nom est " + animalName.getText(), Toast.LENGTH_LONG).show();


        final TextView edv = (TextView) findViewById(R.id.edv);
        edv.setText(monAnimal.getStrHightestLifespan());

        final TextView pdg = (TextView) findViewById(R.id.pdg);
        pdg.setText(monAnimal.getStrGestationPeriod());

        final TextView paln = (TextView) findViewById(R.id.paln);
        paln.setText(monAnimal.getStrBirthWeight());

        final TextView paaa = (TextView) findViewById(R.id.paaa);
        paaa.setText(monAnimal.getStrAdultWeight());

        final ImageView animalImage = (ImageView) findViewById(R.id.image);
        int id = getResources().getIdentifier(monAnimal.getImgFile(),"drawable",getPackageName());
        animalImage.setImageResource(id);

        final EditText sdc = (EditText) findViewById(R.id.sdc);
        sdc.setText(monAnimal.getConservationStatus());

        Button secondButton = (Button) findViewById(R.id.save);
        assert secondButton !=null;

        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
                    public void onClick(View view) {
                monAnimal.setConservationStatus( sdc.getText().toString());
                //Log.d("msg",(String) sdc.getText());

                finish();
            }
        });

    }
}

